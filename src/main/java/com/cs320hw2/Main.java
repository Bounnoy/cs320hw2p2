package com.cs320hw2;

import java.util.Arrays;

public class Main {

    public static Expr[] build(int[] num) {

        int n = num.length;

        if (n == 2)
            return new Expr[] {
                    new Add(new Val(num[0]), new Val(num[1])),
                    new Subtract(new Val(num[0]), new Val(num[1])),
                    new Multiply(new Val(num[0]), new Val(num[1])),
                    new Divide(new Val(num[0]), new Val(num[1]))
            };

        if (n == 3) {
            // Split arrays.
            int[] x1 = Arrays.copyOfRange(num, 0, 1);
            int[] y1 = Arrays.copyOfRange(num, 1, n);

            int[] x2 = Arrays.copyOfRange(num, 0, 2);
            int[] y2 = Arrays.copyOfRange(num, 2, n);

            // Build subexpressions.
            Expr[]  x = build(y1),
                    y = build(x2);

            // Allocate return array.
            Expr[] exp = new Expr[4 * (x.length + y.length)];
            int e = 0; // exp index

            // Populate return array.
            for (int i = 0; i < 4; i++) {

                // Put subexpression x on left side.
                exp[e++] = new Add(x[i], new Val(x1[0]));
                exp[e++] = new Subtract(x[i], new Val(x1[0]));
                exp[e++] = new Multiply(x[i], new Val(x1[0]));
                exp[e++] = new Divide(x[i], new Val(x1[0]));

                // Put subexpression y on right side.
                exp[e++] = new Add(new Val(y2[0]), y[i]);
                exp[e++] = new Subtract(new Val(y2[0]), y[i]);
                exp[e++] = new Multiply(new Val(y2[0]), y[i]);
                exp[e++] = new Divide(new Val(y2[0]), y[i]);
            }

            return exp;
        }

        if (n == 4) {
            // Split arrays.
            int[] x1 = Arrays.copyOfRange(num, 0, 1);
            int[] y1 = Arrays.copyOfRange(num, 1, n);

            int[] x2 = Arrays.copyOfRange(num, 0, 2);
            int[] y2 = Arrays.copyOfRange(num, 2, n);

            int[] x3 = Arrays.copyOfRange(num, 0, 3);
            int[] y3 = Arrays.copyOfRange(num, 3, n);

            // Build subexpressions.
            Expr[]  x = build(y1),
                    a = build(x2),
                    b = build(y2),
                    y = build(x3);

            // Allocate return array.
            Expr[] exp = new Expr[4 * (x.length + (2 * (a.length + b.length)) + y.length)];
            int e = 0; // exp index

            // Populate return array.
            for (int i = 0; i < 32; i++) {

                // Put subexpression x on left side.
                exp[e++] = new Add(x[i], new Val(x1[0]));
                exp[e++] = new Subtract(x[i], new Val(x1[0]));
                exp[e++] = new Multiply(x[i], new Val(x1[0]));
                exp[e++] = new Divide(x[i], new Val(x1[0]));

                // Put subexpression y on right side.
                exp[e++] = new Add(new Val(y3[0]), y[i]);
                exp[e++] = new Subtract(new Val(y3[0]), y[i]);
                exp[e++] = new Multiply(new Val(y3[0]), y[i]);
                exp[e++] = new Divide(new Val(y3[0]), y[i]);
            }

            // Put subexpression a on left side.
            for (int i = 0; i < 4; i++) {

                // Put subexpression b on right side.
                for (int j = 0; j < 4; j++) {
                    exp[e++] = new Add(a[i], b[j]);
                    exp[e++] = new Subtract(a[i], b[j]);
                    exp[e++] = new Multiply(a[i], b[j]);
                    exp[e++] = new Divide(a[i], b[j]);
                }
            }

            return exp;
        }

        return null;
    }

    public static void main(String[] args) {
        int [] numbers = new int[] {10,10,10,10};

        // Generate and save expressions.
        Expr[] exp = build(numbers);

        // Display all valid expressions.
        for (int i = 0; i < 320; i++) {
            int value = exp[i].evaluate();

            if (0 <= value && value <= 9) {
                System.out.println();
                exp[i].display();
                System.out.println(" = " + value);
            }
        }
    }
}
